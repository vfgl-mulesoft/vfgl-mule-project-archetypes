# **Introduction**
This instruction page is to assist in the creation of Mule 4 APIs (EAPI, PAPI, and SAPI) usig the VF-GL Maven archetypes.

# How To?
## Install all archetype templates on developer workstation
To install this project in the local workstation Maven repository, clone this repository and from the cloned directory:

**Follow the steps below:**

* Step 1: Clone the code from the ***VF Bitbucket Git***

  - **Using HTTPS to clone the project:**
    - `git clone https://vfgl-mulesoft/vfgl-mule-project-archetypes.git`

* Step 2: Change the folder to **vfgl-mule-project-archetypes**

> **`cd vfgl-mule-project-archetypes`**

* Step 3: Switch to the most current branch.

> **`git checkout master`**

* Step 4: Install the package into your local Repository

> **`mvn install`**

---
**Sample Output:**

```
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO]
[INFO] VF-GL Archetype Parent 1.0.0 ......................... SUCCESS [  0.766 s]
[INFO] Archetype - Mule Experience API Project 0.0.1 ...... SUCCESS [  3.328 s]
[INFO] Archetype - Mule Process API Project 0.0.1 ......... SUCCESS [  0.469 s]
[INFO] Archetype - Mule System API Project 0.0.1 .......... SUCCESS [  0.453 s]
[INFO] Archetype - Mule Queue-Listening App Project 0.0.1 . SUCCESS [  0.593 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  5.797 s
[INFO] Finished at: 2020-05-29T14:45:45-05:00
[INFO] ------------------------------------------------------------------------   
```

---

## Deploy All Archetypes to Artifact Repository (Optional)

```
mvn deploy -Partifact-repo
```

## Generating a Skeleton Project
Before generating the skeleton and to use the Exchange binary, add the following config into your /.m2/settings.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"          
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <profiles>
    <profile>
      <id>Mule</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <repositories>
        <repository>
          <id>VFExchange</id>
          <name>VF Exchange Repository</name>
          <url>https://maven.anypoint.mulesoft.com/api/v1/organizations/cd609ea1-1c8a-4d06-9d4d-230aca5955d4/maven</url>
          <layout>default</layout>
        </repository>
      </repositories>
    </profile>
  </profiles>
</settings>
```

---

### **To generate an Experience API skeleton using this archetype, use the command:**
```
mvn archetype:generate -DarchetypeGroupId=cd609ea1-1c8a-4d06-9d4d-230aca5955d4 -DarchetypeArtifactId=vfgl-experience-api -DarchetypeVersion=0.0.1 -DartifactId=vfgl-archetype-eapi -DapplicationName="VFGL Archetype Exp API" -B -DbusinessGroup=vfnora
```

## Breakdown:
**mvn archetype:generate -DarchetypeGroupId=cd609ea1-1c8a-4d06-9d4d-230aca5955d4 -DarchetypeArtifactId=vfgl-experience-api...**
- The beginning part of this maven command is copied and used **as-is**.

**-DarchetypeVersion=0.0.1**
- This is the version of the archetype that will be used.  Under normal conditions the newest version of the archetype should be used.

**-DartifactId=archetype-papi**
- This is the lower-case name of the API.
    - Example:
      - _-DartifactId=vfgl-pricing-eapi

**-DapplicationName=VFGL Archetype\ Exp\ API -B**
- This should be the name of the API in Exchange.
  - Since there will be spaces in the name it will have to be encapsulated with single quotes.
    - Example:
      - _-DapplicationName='VFGL Pricing EAPI' -B_

| Parameter       | Description      | Remarks                                                       |
| --------------- | ---------------- | ------------------------------------------------------------- |
| artifactId      | Project Name     | No spaces. All lowercase. Used for package name or filename   |
| applicationName | Application Name | Display name.                                                 |
| groupId         | Group ID         | Used for maven repository. Default: com.vfc.mule.api.exp |
| businessGroup   | Business Group   | Mapped to Anypoint Platform Business Group                    |
| basePath        | Base Path        | For API HTTP Listener base path (Example: **/api/v1/***)      |


### **To generate an Process API skeleton using this archetype, use the command:**
```
mvn archetype:generate -DarchetypeGroupId=cd609ea1-1c8a-4d06-9d4d-230aca5955d4 -DarchetypeArtifactId=vfgl-process-api -DarchetypeVersion=0.0.1 -DartifactId=archetype-papi -DapplicationName=VFGL\ Archetype\ Proc\ API -B
```
## Breakdown:
**mvn archetype:generate -DarchetypeGroupId=cd609ea1-1c8a-4d06-9d4d-230aca5955d4 -DarchetypeArtifactId=vfgl-process-api...**
- The beginning part of this maven command is copied and used **as-is**.

**-DarchetypeVersion=0.0.1**
- This is the version of the archetype that will be used.  Under normal conditions the newest version of the archetype should be used.

**-DartifactId=archetype-papi**
- This is the lower-case name of the API.
    - Example:
      - _-DartifactId=vfgl-shopping-cart-papi_

**-DapplicationName=VFGL\ Archetype\ Proc\ API -B**
- This should be the name of the API in Exchange.
  - Since there will be spaces in the name it will have to be encapsulated with single quotes.
    - Example:
      - _-DapplicationName='VFGL Shopping Cart PAPI' -B_

| Parameter       | Description      | Remarks                                                       |
| --------------- | ---------------- | ------------------------------------------------------------- |
| artifactId      | Project Name     | No spaces. All lowercase. Used for package name or filename   |
| applicationName | Application Name | Display name                                                  |
| groupId         | Group ID         | Used for maven repository. Default: com.vfc.mule.api.prc |
| businessGroup   | Business Group   | Mapped to Anypoint Platform Business Group                    |
| basePath        | Base Path        | For API HTTP Listener base path (Example: **/api/v1/***)      |

### **To generate an System API skeleton using this archetype, use the command:**
```
mvn archetype:generate -DarchetypeGroupId=cd609ea1-1c8a-4d06-9d4d-230aca5955d4 -DarchetypeArtifactId=vfgl-system-api -DarchetypeVersion=0.0.1 -DartifactId=archetype-sapi -DapplicationName=VFGL\ Archetype\ Sys\ API -B
```

## Breakdown:
**mvn archetype:generate -DarchetypeGroupId=cd609ea1-1c8a-4d06-9d4d-230aca5955d4 -DarchetypeArtifactId=vfgl-system-api...**
- The beginning part of this maven command is copied and used **as-is**.

**-DarchetypeVersion=0.0.1**
- This is the version of the archetype that will be used.  Under normal conditions the newest version of the archetype should be used.

**-DartifactId=archetype-papi**
- This is the lower-case name of the API.
    - Example:
      - _-DartifactId=vfgl-ef-email-sapi_

**-DapplicationName=VFGL\ Archetype\ Sys\ API -B**
- This should be the name of the API in Exchange.
  - Since there will be spaces in the name it will have to be encapsulated with single quotes.
    - Example:
      - _-DapplicationName='VFGL Event Framework Email SAPI' -B_

| Parameter       | Description      | Remarks                                                       |
| --------------- | ---------------- | ------------------------------------------------------------- |
| artifactId      | Project Name     | No spaces. All lowercase. Used for package name or filename   |
| applicationName | Application Name | Display name                                                  |
| groupId         | Group ID         | Used for maven repository. Default: com.vfc.mule.api.sys |
| businessGroup   | Business Group   | Mapped to Anypoint Platform Business Group                    |
| basePath        | Base Path        | For API HTTP Listener base path (Example: **/api/v1/***)      |


## **Built With**

* [Mulesoft](http://https://www.mulesoft.com/platform/enterprise-integration) - Anypoint Platform
* [Maven](https://maven.apache.org/) - Dependency Management
* [RAML](http://raml.org) - API Designer

## **Versioning**

We use [BitBucket](http://https://bitbucket.org//) for versioning. For the versions available, see the [tags on this repository example-readme](https://bitbucket.org/vfgl-mulesoft/vfgl-mule-project-archetypes/example-readme.git).
* GitHub URL: https://bitbucket.org/vfgl-mulesoft/vfgl-mule-project-archetypes/example-readme.git

### **FAQ**

#### **How to add Maven Exchange Repository?**
-   Open your Maven Settings (<M2_HOME>/settings.xml)
- 	Add the following config under `<servers` tag for your Exchange Login Credential

```
<servers>
....  
		<server>
		  <id>VFExchange</id>
		  <username><REPLACE_WITH_ANYPOINT_USERNAME></username>
		  <password><REPLACE_WITH_ANYPOINT_PASSWORD></password>
	</server>
...
</servers>
```

#### **How to run multiple applications at the same time (experience, process, system) ?**
-   Make sure the port numbers are different for all the APIs
-   Appropriate client ids and secrets are given in the global properties

#### **How to solve 503 error ?**

-   503 error is caused by a new feature introduced in 4.2.x and later, the API Gatekeeper.
-   Gatekeeper is a process on the Mule server that, by default, blocks any incoming request to the API if the API has not been successfully paired with API Manager to receive appropriate policies. This security setting means that by default - if a policy is not applied, the API cannot be accessed.
-   To avoid the error.
      - While deploying application,
          - Right click on project->Run As -> Run Configurations (This can also be done by clicking on the run tab in the anypoint studio Menu -> Run configurations)
      - Go to Arguments Tab, In VM Arguments add the following command

```
-Danypoint.platform.gatekeeper=disabled
```

-   Click Apply and run the application.

#### **How to solve `MuleEncryptionException` error ?**
-   MuleEncryptionException error is caused becuase the encryption key provided when running the application is different or one is not provided when deploying the application.
-   This secure setting is to provide a encryption feature to the secure data saved in the project when implemented.
-    To avoid the error.
       -   while deploying application, Right click on project->Run As -> Run Configurations (This can also be done by clicking on the run tab in the anypoint studio Menu -> Run configurations)
       -   Go to Arguments Tab, In VM Arguments add the following command

```
       -Dxh.runtime.key=XXXXXXXXXXXXXXXX (length of the key should be minimum of 16 characters)
```

-   Click Apply and run the application


## History

| Date       | Author         | Remarks                             |
| ---------- | -------------- | ------------------------------------|
| 2020-07-20 | Max Girin | Initial Project                     |

## Authors

* **Max Girin** - *MG* - [mgirinast](https://bitbucket.org/mgirinast/)
