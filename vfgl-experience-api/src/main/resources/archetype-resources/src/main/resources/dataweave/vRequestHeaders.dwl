%dw 2.0
output application/java
---
{
	"clientId": attributes.headers["client_id"],
	"clientSecret": attributes.headers["client_secret"],
	"transactionId": attributes.headers["x-transaction-id"] default (correlationId default uuid()),
	"language": attributes.headers["language"],
	"zone":  attributes.headers["zone"],
	"source": attributes.headers["source"]
}