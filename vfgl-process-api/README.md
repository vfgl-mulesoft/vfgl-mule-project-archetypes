# Archetype for Template: vfgl-process-api

Used to generate the archetype for generating a skeleton Process API.

## Install Archetype on Workstation
To install this project in the local workstation Maven repository, clone this repository and from the cloned directory:

```
mvn install
```
## Deploy Archetype to Artifact Repository

```
mvn deploy -Partifact-repo
```

## Generating a Skeleton Project

To generate a Process API skeleton using this archetype, use the command:

```
mvn archetype:generate -DarchetypeGroupId=com.vfc.mule.templates -DarchetypeArtifactId=vfgl-process-api -DarchetypeVersion=1.0.0 -DartifactId=archetype-prc -DapplicationName=VFGL\ Archetype\ Prc\ API -B
```
