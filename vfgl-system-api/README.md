# Archetype for Template: vfgl-system-api

Used to generate the archetype for generating a skeleton System API.

## Install Archetype on Workstation
To install this project in the local workstation Maven repository, clone this repository and from the cloned directory:

```
mvn install
```
## Deploy Archetype to Artifact Repository

```
mvn deploy -Partifact-repo
```

## Generating a Skeleton Project

To generate a System API skeleton using this archetype, use the command:

```
mvn archetype:generate -DarchetypeGroupId=com.vfc.mule.templates -DarchetypeArtifactId=vfgl-system-api -DarchetypeVersion=1.0.0 -DartifactId=archetype-sapi -DapplicationName=VFGL\ Archetype\ Sys\ API -B
```
