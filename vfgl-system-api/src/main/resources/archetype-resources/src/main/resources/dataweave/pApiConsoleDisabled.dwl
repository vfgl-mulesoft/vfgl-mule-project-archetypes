%dw 2.0
output application/json
---
{
  "errorCode": "HTTP-404",
  "transactionId": attributes.headers['x-transaction-id'],
  "message": "Resource not found",
  "messageLocal": "资源未找到",
  "status":" FAILED"
}